
## Редактирование таблиц
 
 
# RENAME-запросы
ALTER TABLE rentcardatabase.carz 
RENAME TO  rentcardatabase.cars;

ALTER TABLE rentcardatabase.buking 
RENAME TO  rentcardatabase.booking;


# Запросы на изменение таблиц
ALTER TABLE rentcardatabase.clients 
CHANGE COLUMN full_name fullname varchar(60) NOT NULL;

ALTER TABLE rentcardatabase.defects 
DROP COLUMN fake_id;

ALTER TABLE rentcardatabase.positions
CHANGE COLUMN title title VARCHAR(45) NOT NULL ;

ALTER TABLE rentcardatabase.staff 
DROP COLUMN patronymic,
DROP COLUMN surname,
CHANGE COLUMN name fullname VARCHAR(60) NOT NULL ;

ALTER TABLE rentcardatabase.booking 
CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT ;

ALTER TABLE rentcardatabase.cars_to_defects 
ADD COLUMN id_cars INT NOT NULL AFTER id_defects,
DROP PRIMARY KEY,
ADD PRIMARY KEY (id_defects, id_cars);

ALTER TABLE rentcardatabase.cars_to_defects 
ADD CONSTRAINT fk_cars_to_defects_cars
FOREIGN KEY (id_cars)
REFERENCES rentcardatabase.cars (id);

ALTER TABLE rentcardatabase.lease_agreement 
ADD COLUMN pledge int NOT NULL;

ALTER TABLE rentcardatabase.technical_maintenance 
CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT ;

ALTER TABLE rentcardatabase.technical_maintenance 
ADD COLUMN list_of_works varchar(100) NOT NULL;









