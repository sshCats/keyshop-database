
# Удаление таблиц и базы данных

DROP TABLE cars;
DROP TABLE clients;
DROP TABLE defects;
DROP TABLE options;
DROP TABLE parking;
DROP TABLE positions;
DROP TABLE staff;
DROP TABLE booking;
DROP TABLE cars_to_defects;
DROP TABLE cars_to_parking;
DROP TABLE lease_agreement;
DROP TABLE options_to_cars;
DROP TABLE technical_maintenance;

DROP DATABASE rentcardatabase;