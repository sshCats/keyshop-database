# Создание базы данных
CREATE DATABASE  IF NOT EXISTS rentcardatabase;
USE rentcardatabase;


# Создание таблиц
DROP TABLE IF EXISTS cars;
CREATE TABLE cars (
  id int NOT NULL AUTO_INCREMENT,
  brand varchar(45) NOT NULL,
  model varchar(45) NOT NULL,
  registration_number varchar(10) DEFAULT NULL,
  VIN varchar(17) NOT NULL,
  year_of_release year NOT NULL,
  mileage int unsigned NOT NULL,
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
  id int NOT NULL AUTO_INCREMENT,
  fullname varchar(60) NOT NULL,
  date_of_birth date NOT NULL,
  passport varchar(12) NOT NULL,
  place_of_registration varchar(100) NOT NULL,
  drivers_license varchar(14) NOT NULL,
  phone_number varchar(16) NOT NULL,
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS defects;
CREATE TABLE defects (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(45) NOT NULL,
  description mediumtext,
  PRIMARY KEY (id)
);


DROP TABLE IF EXISTS options;
CREATE TABLE options (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(45) NOT NULL,
  description mediumtext,
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS parking;
CREATE TABLE parking (
  id int NOT NULL AUTO_INCREMENT,
  address varchar(100) NOT NULL,
  PRIMARY KEY (id)
);


DROP TABLE IF EXISTS positions;
CREATE TABLE positions (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(45) NOT NULL,
  salary int unsigned NOT NULL,
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS staff;
CREATE TABLE staff (
  id int NOT NULL AUTO_INCREMENT,
  fullname varchar(60) NOT NULL,
  date_of_birth date NOT NULL,
  passport varchar(12) NOT NULL,
  place_of_registration varchar(100) NOT NULL,
  date_of_employment date NOT NULL,
  date_of_dismissal varchar(45) DEFAULT NULL,
  id_positions int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_staff_position_idx (id_positions),
  CONSTRAINT fk_staff_position FOREIGN KEY (id_positions) REFERENCES positions (id)
);

DROP TABLE IF EXISTS booking;
CREATE TABLE booking (
  id int NOT NULL AUTO_INCREMENT,
  rental_duration varchar(21) NOT NULL,
  payment_method varchar(45) NOT NULL,
  date_and_time datetime NOT NULL,
  pledge int NOT NULL,
  id_cars int NOT NULL,
  id_clients int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_booking_cars_idx (id_cars),
  KEY fk_booking_clients_idx (id_clients),
  CONSTRAINT fk_booking_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_booking_clients FOREIGN KEY (id_clients) REFERENCES clients (id)
);

DROP TABLE IF EXISTS cars_to_defects;
CREATE TABLE cars_to_defects (
  id_cars int NOT NULL,
  id_defects int NOT NULL,
  PRIMARY KEY (id_cars,id_defects),
  KEY fk_cars_to_defects_cars_idx (id_defects),
  KEY fk_cars_to_defects_defects_idx (id_defects),
  CONSTRAINT fk_cars_to_defects_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_cars_to_defects_defects FOREIGN KEY (id_defects) REFERENCES defects (id)
);

DROP TABLE IF EXISTS cars_to_parking;
CREATE TABLE cars_to_parking (
  id_cars int NOT NULL,
  id_parking int NOT NULL,
  PRIMARY KEY (id_cars,id_parking),
  KEY fk_cars_to_parking_cars_idx (id_cars),
  KEY fk_cars_to_parking_parking_idx (id_parking),
  CONSTRAINT fk_cars_to_parking_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_cars_to_parking_parking FOREIGN KEY (id_parking) REFERENCES parking (id)
);

DROP TABLE IF EXISTS lease_agreement;
CREATE TABLE lease_agreement (
  id int NOT NULL AUTO_INCREMENT,
  contract_number varchar(12) NOT NULL,
  date_of_signing date NOT NULL,
  rental_duration varchar(21) NOT NULL,
  pledge int NOT NULL,
  price int NOT NULL,
  payment_method varchar(45) NOT NULL,
  id_staff int NOT NULL,
  id_cars int NOT NULL,
  id_clients int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_lease_agreement_staff_idx (id_staff),
  KEY fk_lease_agreement_cars_idx (id_cars),
  KEY fk_lease_agreement_clients_idx (id_clients),
  CONSTRAINT fk_lease_agreement_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_lease_agreement_clients FOREIGN KEY (id_clients) REFERENCES clients (id),
  CONSTRAINT fk_lease_agreement_staff FOREIGN KEY (id_staff) REFERENCES staff (id)
);

DROP TABLE IF EXISTS options_to_cars;
CREATE TABLE options_to_cars (
  id_cars int NOT NULL,
  id_options int NOT NULL,
  PRIMARY KEY (id_cars,id_options),
  KEY fk_options_to_cars_cars_idx (id_options),
  KEY fk_options_to_cars_options_idx (id_options),
  CONSTRAINT fk_options_to_cars_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_options_to_cars_options FOREIGN KEY (id_options) REFERENCES options (id)
);

DROP TABLE IF EXISTS technical_maintenance;
CREATE TABLE technical_maintenance (
  id int NOT NULL AUTO_INCREMENT,
  service_name varchar(60) NOT NULL,
  list_of_works varchar(100) NOT NULL,
  service_date date NOT NULL,
  cars_mileage int NOT NULL,
  id_cars int DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_technical_maintenance_cars_idx (id_cars),
  CONSTRAINT fk_technical_maintenance_cars FOREIGN KEY (id_cars) REFERENCES cars (id)
);

# Запросы на добавление данных
INSERT INTO rentcardatabase.cars (brand, model, registration_number, VIN, year_of_release, mileage) VALUES ('Audi', 'A6', 'Е912АР 134', 'JA4LX31G93U065670', 2020, 10);
INSERT INTO rentcardatabase.clients (fullname, date_of_birth, passport, place_of_registration, drivers_license, phone_number) VALUES ('Ижев Федор Григорьевич', '1995-01-17', '8365 264875', 'г.Самара, ул.Солнечная, д.20, кв.130', '22 02 254682', '8 920 748 56 34');
INSERT INTO rentcardatabase.booking (rental_duration, payment_method, date_and_time, pledge, id_cars, id_clients) VALUES ('28.06.2021-30.06.2021', 'Visa', '2021-06-28 11:00:00', '2500', '1', '1');
INSERT INTO rentcardatabase.defects (title, description) VALUES ('Царапина на правой двери', 'Имеется небольшая царапина в нижней части правой двери. Необходимо исправить');
INSERT INTO rentcardatabase.cars_to_defects (id_cars, id_defects) VALUES ('1', '1');
INSERT INTO rentcardatabase.parking (address) VALUES ('ул.Пушкина д.Колотушкина');
INSERT INTO rentcardatabase.cars_to_parking (id_cars, id_parking) VALUES ('1', '1');
INSERT INTO rentcardatabase.positions (title, salary) VALUES ('Менеджер по работе с клиентами', '35000');
INSERT INTO rentcardatabase.staff (fullname, date_of_birth, passport, place_of_registration, date_of_employment, id_positions) VALUES ('Иво Кирилл Андреевич', '1998-10-27', '9476 385612', 'г.Волгоград, ул.Пельше, д.15, кв.23', '2000-01-05', '1');
INSERT INTO rentcardatabase.technical_maintenance (service_name, list_of_works, service_date, cars_mileage, id_cars) VALUES ('SuperCarRepair', 'Удаление царапины. Замена лампочки', '2021-03-19', '2157', '1');
