# Создание базы данных
CREATE DATABASE  IF NOT EXISTS rentcardatabase;
USE rentcardatabase;


# Создание таблиц
DROP TABLE IF EXISTS carz;
CREATE TABLE carz (
  id int NOT NULL AUTO_INCREMENT,
  brand varchar(45) NOT NULL,
  model varchar(45) NOT NULL,
  registration_number varchar(10) DEFAULT NULL,
  VIN varchar(17) NOT NULL,
  year_of_release year NOT NULL,
  mileage int unsigned NOT NULL,
  PRIMARY KEY (id)
);
# EDIT
ALTER TABLE rentcardatabase.carz 
RENAME TO  rentcardatabase.cars;

DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
  id int NOT NULL AUTO_INCREMENT,
  full_name varchar(60) NOT NULL,
  date_of_birth date NOT NULL,
  passport varchar(12) NOT NULL,
  place_of_registration varchar(100) NOT NULL,
  drivers_license varchar(14) NOT NULL,
  phone_number varchar(16) NOT NULL,
  PRIMARY KEY (id)
);
# EDIT
ALTER TABLE rentcardatabase.clients 
CHANGE COLUMN full_name fullname varchar(60) NOT NULL;

DROP TABLE IF EXISTS defects;
CREATE TABLE defects (
  id int NOT NULL AUTO_INCREMENT,
  fake_id int NOT NULL,
  title varchar(45) NOT NULL,
  description mediumtext,
  PRIMARY KEY (id)
);
# EDIT
ALTER TABLE rentcardatabase.defects 
DROP COLUMN fake_id;


DROP TABLE IF EXISTS options;
CREATE TABLE options (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(45) NOT NULL,
  description mediumtext,
  PRIMARY KEY (id)
);

DROP TABLE IF EXISTS parking;
CREATE TABLE parking (
  id int NOT NULL AUTO_INCREMENT,
  address varchar(100) NOT NULL,
  PRIMARY KEY (id)
);


DROP TABLE IF EXISTS positions;
CREATE TABLE positions (
  id int NOT NULL AUTO_INCREMENT,
  title varchar(20) NOT NULL,
  salary int unsigned NOT NULL,
  PRIMARY KEY (id)
);
# EDIT
ALTER TABLE rentcardatabase.positions
CHANGE COLUMN title title VARCHAR(45) NOT NULL ;

DROP TABLE IF EXISTS staff;
CREATE TABLE staff (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(45) NOT NULL,
  surname varchar(45) NOT NULL,
  patronymic varchar(45) NOT NULL,
  date_of_birth date NOT NULL,
  passport varchar(12) NOT NULL,
  place_of_registration varchar(100) NOT NULL,
  date_of_employment date NOT NULL,
  date_of_dismissal varchar(45) DEFAULT NULL,
  id_positions int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_staff_position_idx (id_positions),
  CONSTRAINT fk_staff_position FOREIGN KEY (id_positions) REFERENCES positions (id)
);
# EDIT
ALTER TABLE rentcardatabase.staff 
DROP COLUMN patronymic,
DROP COLUMN surname,
CHANGE COLUMN name fullname VARCHAR(60) NOT NULL ;

DROP TABLE IF EXISTS buking;
CREATE TABLE buking (
  id int NOT NULL,
  rental_duration varchar(21) NOT NULL,
  payment_method varchar(45) NOT NULL,
  Date_and_time datetime NOT NULL,
  pledge int NOT NULL,
  id_cars int NOT NULL,
  id_clients int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_booking_cars_idx (id_cars),
  KEY fk_booking_clients_idx (id_clients),
  CONSTRAINT fk_booking_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_booking_clients FOREIGN KEY (id_clients) REFERENCES clients (id)
);
# EDIT
ALTER TABLE rentcardatabase.buking 
RENAME TO  rentcardatabase.booking;
# EDIT
ALTER TABLE rentcardatabase.booking 
CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT ;

DROP TABLE IF EXISTS cars_to_defects;
CREATE TABLE cars_to_defects (
  id_defects int NOT NULL,
  PRIMARY KEY (id_defects),
  KEY fk_cars_to_defects_defects_idx (id_defects),
  CONSTRAINT fk_cars_to_defects_defects FOREIGN KEY (id_defects) REFERENCES defects (id)
);
# EDIT
ALTER TABLE rentcardatabase.cars_to_defects 
ADD COLUMN id_cars INT NOT NULL AFTER id_defects,
DROP PRIMARY KEY,
ADD PRIMARY KEY (id_defects, id_cars);
# EDIT
ALTER TABLE rentcardatabase.cars_to_defects 
ADD CONSTRAINT fk_cars_to_defects_cars
FOREIGN KEY (id_cars)
REFERENCES rentcardatabase.cars (id);

DROP TABLE IF EXISTS cars_to_parking;
CREATE TABLE cars_to_parking (
  id_cars int NOT NULL,
  id_parking int NOT NULL,
  PRIMARY KEY (id_cars,id_parking),
  KEY fk_cars_to_parking_cars_idx (id_cars),
  KEY fk_cars_to_parking_parking_idx (id_parking),
  CONSTRAINT fk_cars_to_parking_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_cars_to_parking_parking FOREIGN KEY (id_parking) REFERENCES parking (id)
);

DROP TABLE IF EXISTS lease_agreement;
CREATE TABLE lease_agreement (
  id int NOT NULL AUTO_INCREMENT,
  contract_number varchar(12) NOT NULL,
  date_of_signing date NOT NULL,
  rental_duration varchar(21) NOT NULL,
  price int NOT NULL,
  payment_method varchar(45) NOT NULL,
  id_staff int NOT NULL,
  id_cars int NOT NULL,
  id_clients int NOT NULL,
  PRIMARY KEY (id),
  KEY fk_lease_agreement_staff_idx (id_staff),
  KEY fk_lease_agreement_cars_idx (id_cars),
  KEY fk_lease_agreement_clients_idx (id_clients),
  CONSTRAINT fk_lease_agreement_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_lease_agreement_clients FOREIGN KEY (id_clients) REFERENCES clients (id),
  CONSTRAINT fk_lease_agreement_staff FOREIGN KEY (id_staff) REFERENCES staff (id)
);
# EDIT
ALTER TABLE rentcardatabase.lease_agreement 
ADD COLUMN pledge int NOT NULL;

DROP TABLE IF EXISTS options_to_cars;
CREATE TABLE options_to_cars (
  id_cars int NOT NULL,
  id_options int NOT NULL,
  PRIMARY KEY (id_cars,id_options),
  KEY fk_options_to_cars_cars_idx (id_options),
  KEY fk_options_to_cars_options_idx (id_options),
  CONSTRAINT fk_options_to_cars_cars FOREIGN KEY (id_cars) REFERENCES cars (id),
  CONSTRAINT fk_options_to_cars_options FOREIGN KEY (id_options) REFERENCES options (id)
);

DROP TABLE IF EXISTS technical_maintenance;
CREATE TABLE technical_maintenance (
  id int NOT NULL,
  service_name varchar(60) NOT NULL,
  service_date date NOT NULL,
  cars_mileage int NOT NULL,
  id_cars int DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_technical_maintenance_cars_idx (id_cars),
  CONSTRAINT fk_technical_maintenance_cars FOREIGN KEY (id_cars) REFERENCES cars (id)
);
# EDIT
ALTER TABLE rentcardatabase.technical_maintenance 
CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT ;
# EDIT
ALTER TABLE rentcardatabase.technical_maintenance 
ADD COLUMN list_of_works varchar(100) NOT NULL;
