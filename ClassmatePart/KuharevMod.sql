/*
1. Можно забронировать несколько машин -> реализовать таблицу car_to_booking
2. У машины может быть только 1 опция -> удалить таблицу options_to_car
3. При создании договора аренды в поле даты автоматически проставляется текущая дата
*/

# 1
alter table booking
drop column id_cars,
drop constraint fk_booking_cars;

drop table if exists booking_to_car;
create table booking_to_car(
	id_booking int not null,
    id_car int not null,
    primary key(id_booking,id_car),
    constraint fk_booking_to_car_booking foreign key (id_booking) references booking (id),
	constraint fk_booking_to_car_car foreign key (id_car) references cars (id)
);


# 2
drop table options_to_cars;

alter table cars
add column id_options int default null after mileage,
add constraint fk_cars_options foreign key (id_options) references options (id);


# 3
alter table lease_agreement
change column date_of_signing date_of_signing date not null default (curdate());


# Необходимый минимум
INSERT INTO rentcardatabase.positions (title, salary) VALUES ('Менеджер по работе с клиентами', '35000');
INSERT INTO rentcardatabase.options (title, description) VALUES ('Детское кресло','Детское кресло для безопасности ребенка');
INSERT INTO rentcardatabase.staff (fullname, date_of_birth, passport, place_of_registration, date_of_employment, id_positions) VALUES ('Иво Кирилл Андреевич', '1998-10-27', '9476 385612', 'г.Волгоград, ул.Пельше, д.15, кв.23', '2000-01-05', '1');
INSERT INTO rentcardatabase.clients (fullname, date_of_birth, passport, place_of_registration, drivers_license, phone_number) VALUES ('Ижев Федор Григорьевич', '1995-01-17', '8365 264875', 'г.Самара, ул.Солнечная, д.20, кв.130', '22 02 254682', '8 920 748 56 34');

# 1-2
INSERT INTO rentcardatabase.booking (rental_duration, payment_method, date_and_time, pledge, id_clients) VALUES ('28.06.2021-30.06.2021', 'Visa', '2021-06-28 11:00:00', '2500', '1');
INSERT INTO rentcardatabase.cars (brand, model, registration_number, VIN, year_of_release, mileage, id_options) VALUES ('Audi', 'A6', 'Е912АР 134', 'JA4LX31G93U065670', 2020, 10, 1),('Kia', 'Rio', 'Е865АР 134', '4USBT53544LT26841', 2018, 45500, 1),('BMW', 'm5', 'Е287АР 134', 'ZFA18800004473122', 2020, 12251, 1);
INSERT INTO rentcardatabase.booking_to_car VALUES (1,1), (1,2), (1,3);

# 3
INSERT INTO rentcardatabase.lease_agreement (contract_number, rental_duration, pledge, price, payment_method, id_staff, id_cars, id_clients) VALUES ('1001/2021', '1', '15000', '7000', 'Наличные', '1', '1', '1');
