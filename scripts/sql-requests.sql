use keyshopdatabase;

# 1 ------------------------------------------------------------------------------------

# 1. Добавить товар в заказ
update testshop.keys set order_id=5 where key_id=1;
update testshop.keys set order_id=5 where hashkey='h-znd-iohy';

# 2. Удалить товар из заказа
update testshop.keys set order_id=null where hashkey='h-znd-iohy';

# 3. Добавить скидку на товар
update testshop.orders set discount_id=1 where order_id=2;

# 4. Удалить скидку на товар
update testshop.orders set discount_id=null where order_id=2;

# 5. Изменить цену игры
update testshop.games set price=290 where title='Terraria';

# 6. Вывести все активные ключи
select * from keyz where order_id is null;

# 7. Вывести все игры конкретной платформы
select game_id, title, price from testshop.games
join testshop.game_to_platform using(game_id)
join testshop.game_platforms using(platform_id)
where name='Linux';

# 8. Вывести все заказы с определенным способом оплаты
select * from testshop.orders where payment_method='Visa';

# 9. Показать отзывы к конкретной игре
select key_id,responses.date,text,rate from testshop.responses
join testshop.keys using(key_id)
join testshop.keys_packs using(pack_id)
join testshop.games using(game_id) where title='Minecraft Java Ed.';

# 10. Показать обращения за определенный день
select * from testshop.appeals where appeal_date='2021-05-01';

# 11. Показать обращения, рассмотренные конкретным администратором
select appeal_id,appeal_date,review_date,text,client_id from testshop.appeals
join testshop.administrators using(administrator_id)
where fullname='Ronald B. Cornwell and status=1';

# 12. Предоставить информацию о наиболее продаваемых играх за месяц
select title, count(game_id) as number_of_purchases from orders
join keyz using(order_id)
join keys_packs using(pack_id)
join games using(game_id)
where orders.date between '2021-05-01' and '2021-05-31'
group by(game_id)
limit 3;

# 13. Предоставить информацию о непопулярных  играх за месяц
select title, count(game_id) as number_of_purchases from orders
join keyz using(order_id)
join keys_packs using(pack_id)
join games using(game_id)
group by(game_id)
order by number_of_purchases
limit 0, 3;

# 14. Предоставить информацию о количестве обращений за месяц
select count(*) from appeals
where appeal_date between '2021-05-01' and '2021-05-31';

# 15. Предоставить информацию о средней цене совершаемого заказа
select avg(price) as average_price from orders;

# 16. Предоставить общие доходы за месяц
select sum(price) as revenue_for_may from orders
where date between '2021-05-01' and '2021-05-31';

# 17. Предоставить общее количество проданных ключей
select count(*) as sold_keys from keyz
where order_id is not null;

# 18. Предоставить оставшееся количество ключей конкретной игры
select count(*) as remaining_keys from keyz
join keys_packs using(pack_id)
join games using(game_id)
where order_id is null and title='Terraria';

# 2 ------------------------------------------------------------------------------------

# 19. Изменение цены
update games
set price=2500
where title='Stalker 2';

# 20. Изменение пароля и баланса на аккаунте
update clients
set password='_indens_',
balance=500
where login='Wasco1991';

# 21. Добавление ключа к заказу
update keyz
set order_id=5
where key_id=8;

# 22. Исправление ошибки в названии
update publishers
set name='Bethesda Softworks'
where publisher_id=1;

# 23. Изменение возраста и графика работы админа
update administrators
set age=26,
shedule_id=2
where fullname='Ronald B. Cornwell';

# 3 ------------------------------------------------------------------------------------


# 24. Удаление слишком больших скидок
delete from discounts
where amount > 25;

# 25. Удаление отрицаельных отзывов
delete from responses
where rate < 4;

#26. Удаление старых отзывов
delete from responses
where date between '2021-04-01' and '2021-04-31'; 

# 27. Удаление приобретенных ключей
delete from keyz
where order_id is not null;

#28. Удаление рассмотренных обращений
delete from appeals
where status = 1;

# 4 ------------------------------------------------------------------------------------

# 29. Поиск всех админов старше 25 лет и работающих по первому графику
select * from administrators
where age > 25 and shedule_id = 1;

# 30. Поиск всех админов, которым между 25 и 30
select * from administrators
where age between 25 and 30;

# 31. Вывод игр, которые были куплены хотя бы 1 раз
select distinct title from keyz
join keys_packs using(pack_id)
join games using(game_id)
where order_id is not null;

# 32. Вывод игр, которые ни разу не купили
select distinct title from keyz
join keys_packs using(pack_id)
join games using(game_id)
where order_id is null and title not in
(
	select distinct title from keyz
	join keys_packs using(pack_id)
	join games using(game_id)
	where order_id is not null
);

# 33. Вывод заказов
select * from orders
where payment_method = 'Visa' or price > 2000;

# 34. Вывод игр, которые выпускаются под мобилки
select distinct title from games
join game_to_platform using(game_id)
join game_platforms using(platform_id)
where name in ('Android', 'iOS');

# 35. Вывод  игр от компании Ubisoft
select title from games
join publisher_to_game using(game_id)
join publishers using(publisher_id)
where name in ('Ubisoft');

# 36. Вывод разработчиков из США и специальной почтой
select name from developers
where country = 'USA' and email like '%.net';

# 37. Вывод админов, кто начинает работать в понедельник либо в среду
select administrator_id, fullname from administrators
join work_shedules using(shedule_id)
where first_day = 'Monday' or first_day = 'Wednesday';

# 38. Вывод игр, вышедших в период между 2005 и 2015
select title from games
where year(release_date) between 2005 and 2015;

# 39. Вывод игр, вышедших не зимой
select title, monthname(release_date) as month from games
where month(release_date) not in (12, 01, 02);

# 40. Вывод игр, вышедших в том же году, что и Террария
select title, release_date from games
where year(release_date) = (
	select year(release_date) from games
	where title = 'Terraria'
);

# 41. Вывод положительных отзывов
select * from responses
where rate between 4 and 5;

# 42. Вывод количества отзывов, обработанными двумя админами
select count(*) from appeals
join administrators using(administrator_id)
where fullname in ('Ronald B. Cornwell', 'Lon T. Ward');

# 43. Подсчет количества разработчиков не из США
select count(*) from developers
where country != 'USA';

# 44. Вывод максимальной цены
select max(price) from games
join developers using(developer_id)
where country != 'USA';

# 45. Вывод заказов, где скидка больше 10 процентов
select order_id, price from orders
join discounts using(discount_id)
where amount > 10;

# 46. Вывод заказов, где скидка от 10 до 15 процентов либо цена меньше 2000
select * from orders
join discounts using(discount_id)
where amount between 10 and 15 or price < 2000;

# 47. Вывод ключей из заказов 2, 3 и 4
select key_id, order_id, date from keyz
join keys_packs using(pack_id)
where pack_id between 2 and 4;

# 48. Вывод некупленных ключей по игре Stalker 2 или игр, стоящих меньше 1000
select key_id, hashkey, title from keyz
join keys_packs using(pack_id)
join publishers using(publisher_id)
join games using (game_id)
where (price < 1000 or title = 'Stalker 2') and order_id is null;

# 5 ------------------------------------------------------------------------------------

# 49. Поиск почтовых адресов, заканчивающихся на ".net"
select * from developers
where email like '%.net';

# 50. Поиск обращений по ключевым словам
select * from appeals
where text like '%fix%' or '%dont work%';

# 51. Форматированный вывод "Игра - разработчик"
select concat(title,' - ',name) as 'Game - Developer' from games
join developers using(developer_id);

# 52. Форматированный вывод необработанных обращений
select concat_ws(' | ', login, appeal_id, text) as 'pending appeals' from appeals
join clients using(client_id)
where status = 0;

# 53. Поиск клиентов с ненадежным паролем
select * from clients
where length(password) < 5;

# 6 ------------------------------------------------------------------------------------

# 54. Создание бэкапа для клиентов с крупным балансом
create table rich_clients like clients;
insert into rich_clients (login, password, balance)
select login, password, balance from clients 
where balance > 2000;

# 55. Создание простого бэкапа всех заказов
create table orders_backups like orders;
insert into orders_backups
select * from orders;

# 8 ------------------------------------------------------------------------------------

# 56. Сортировка админов по расписанию
select * from administrators
order by shedule_id desc;

# 57. Вывод количества рассмотренных заявок каждым админом
select fullname, count(*) as number from appeals
natural join administrators
group by administrator_id;

# 58. Вывод количества игр по странам
select country, count(title) as number from games
join developers using(developer_id)
group by country;

# 59. Подсчет количества обращений от каждого из клиентов
select login, count(*) as number from appeals
natural join clients
group by client_id
order by number desc;

# 60. Подсчет количества обращений по дням
select appeal_date, count(*) as number from appeals
group by appeal_date
order by number desc;

# 61. Топ 3 стран по количеству разработчиков
select country, count(*) as number from developers
group by country
order by number desc
limit 3;

# 62. Средний баланс на счетах клиентов
select avg(balance) as average from clients;

# 63. Общая прибыль
select sum(price) as sum from orders;

# 64. Топ прибыли по способам оплаты
select payment_method, (price) as sum from orders
group by payment_method
having sum is not null
order by sum desc;

# 65. Среднее количество издателей на игру
select avg(number) as average from (
	select count(*) as number from games
	natural join publisher_to_game
	natural join publishers
	group by title
) as number;

# 66. Топ 5 игр по количеству издателей
select title, count(*) as number from games
natural join publisher_to_game
group by game_id
order by number desc
limit 5;

# 67. Сортировка отзывов
select * from responses
order by date asc;

# 68. Отзывы, отсортированные по алфавиту(Игра) и оценке
select key_id, text, rate, title from responses
join keyz using(key_id)
join keys_packs using(pack_id)
join games using(game_id)
order by title, rate desc;

# 69. Количество игр по странам, вышедших позднее 2015 года
select country, count(*) as number, release_date from games
natural join developers
group by country
having year(release_date) > 2015;

# 70. Вывод общего количества оставшихся ключей
select count(*) as 'remaining keys' from keyz;

# 71. Вывод максимального числа ключей в поставке
select max(number) as average from (
	select count(*) as number from keyz
	group by pack_id
) as number;

# 72. Вывод платформ по играм с убыванием
select name, count(*) as number from game_platforms
join game_to_platform using(platform_id)
join games using(game_id)
group by name
order by number;

# 73. Вывод минимальный цены игры у платформы PlayStation 5
select min(price) from games
join game_to_platform using(game_id)
join game_platforms using(platform_id)
where name = 'PlayStation 5';

# 74. Вывод игр, чья цена меньше средней, и разницы между ценой и средней
select title, price, (
	select avg(price) from games
)-price as delta from games
where price < (
	select avg(price) from games
);

# 75. Вывод ближайшего года, когда выпустилась игра на PlayStation 5
select max(years) from (
	select year(release_date)as years from games
	join game_to_platform using(game_id)
	join game_platforms using(platform_id)
	where name = 'PlayStation 5'
) as y;

# 9 ------------------------------------------------------------------------------------

# 76. Вывод обращений и отзывов вместе
select appeal_date as date, text from appeals
union
select date, text from responses
order by 1;

# 77. Общий список разработчиков и издателей
select name from developers
union
select name from publishers
order by name;

# 10 -----------------------------------------------------------------------------------

# 78. Вывод клиентов, которые могут приобрести любую игру
select * from clients
where balance > all(
	select price from games
);

# 79. Вывод клиентов, которые могут приобрести хотя бы самую дешевую игру
select * from clients
where balance > any(
	select price from games
);

# 80. Вывод клиентов, которые хотя бы раз делал заказ
select * from clients c
where exists (
select * from orders o
where

# 11 -----------------------------------------------------------------------------------

# 81. Получить все платежные системы, которые использовались за последний месяц
select group_concat(distinct payment_method) from orders
where date between '2021-05-01' and '2021-05-31';

# 82. Получить всех клиентов, чей баланс больше 2000
select group_concat(login) as login from clients
where balance>2000;

# 83. Выборка разработчиков по интересующим странам
select country, group_concat(name) as name from developers
where country in ('USA', 'Ukraine')
group by(country);

# 84. Вывод всех кодов, использовавшихся при оплате Visa
select group_concat(distinct promo_code) as promocodes from orders
join discounts using(discount_id)
where payment_method = 'Visa';

# 85. Вывод еще не обработанных обращений по датам
select appeal_date, group_concat(appeal_id) as id from appeals
where status=0
group by(appeal_date);

# 7 ------------------------------------------------------------------------------------

# 86. Вывод клиентов вместе с их заказами
select client_id, login, date, order_id from clients
left join orders using(client_id);

# 87. Вывод клиентов и их отзывов
select client_id, login, key_id, text from clients
join orders using(client_id)
join keyz using(order_id)
left join responses using(key_id);

# 88. Вывод заказов и скидок
select order_id, date, promo_code from orders
left join discounts using(discount_id);

# 89. Вывод расписаний и админов, которые к ним прикреплены
select * from administrators
right join work_shedules using(shedule_id);

# 90. Вывод админов и отзывов, которые они  рассматривали
select * from administrators
left join appeals using(administrator_id);

# 91. Вывод админов и отзывов, которые они могли бы рассмотреть
select fullname, appeal_id, text from administrators
cross join appeals
where status = 0;

# 92. Вывод всевозможных комбинаций "Игра-издатель"
select concat_ws(' - ', name, title) as 'Publisher - Game' from publishers
join games;

# 93. Вывод игр и платформ, на которые они могли бы выйти
select concat_ws(' - ', title, name) as 'Game - Potential platform' from games
join game_platforms
where (game_id, platform_id) not in (
	select * from game_to_platform
)
order by(game_id);

# 94. Вывод ключей от отзывов к ним
select key_id, date, text, rate from keyz
left join responses using(key_id)

# 95. Вывод админов и поставок, которые они принимали
select fullname, pack_id, date from administrators
left join keys_packs using(administrator_id)

# 96. Вывод обращений клиентов вместе с админами
select login as Client, text, status, fullname as Admin from clients
join appeals using(client_id)
left join administrators using(administrator_id)
order by(client_id)

# MOD ----------------------------------------------------------------------------------
1. Вывод топ 5 игр по их средней оценке
select title, round(avg(rate), 1) as 'Average rate' from games
join keys_packs using(game_id)
join keyz using(pack_id)
join responses using(key_id)
group by(title)
order by(`Average rate`) desc
limit 5;

2. Вывод сводки по цене, которую потратили клиенты за месяц
select client_id, login, round(sum(games.price*(1-amount))) as 'monthly expenses',
group_concat(distinct orders.date) as 'purchase dates' from clients
join orders using(client_id)
join discounts using(discount_id)
join keyz using(order_id)
join keys_packs using(pack_id)
join games using(game_id)
where orders.date between '2021-05-01' and '2021-05-31'
group by(client_id);

3. Сводная таблица по играм
SELECT * FROM testshop.clients;
select game_id, title, release_date, price, T.avg as 'Average rate', developers.name
country, group_concat(distinct game_platforms.name) as 'Game platforms',
group_concat(distinct publishers.name) as 'Publishers', T.sum as 'Remaining keys' from games
join developers using(developer_id)
join game_to_platform using(game_id)
join game_platforms using(platform_id)
join publisher_to_game using(game_id)
join publishers using(publisher_id)
join (
	select game_id, round(avg(rate), 1) as avg, sum(order_id is null) as sum from keys_packs
	join keyz using(pack_id)
	left join responses using(key_id)
	group by(game_id)
) as T using(game_id)
group by(title)
order by(title);
