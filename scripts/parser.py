from bs4 import BeautifulSoup
from random import randint
import requests
from threading import Thread, enumerate
from time import sleep, time

counter = 1
data = [[] for i in range(1000)]
header = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'}
base_path = r'C:/Users/hacknorthkorea/Desktop/MySQL_DataCSV/'


class MyThread(Thread):
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self.mul = 0
    
    def setMul(self, mul):
        self.mul = mul
    
    def run(self):
        writeAdministrators(self.mul, 20)


def create_threads():
    for i in range(50):
        name = f'Thread #{(i+1)}'
        my_thread = MyThread(name)
        my_thread.setMul(i)
        my_thread.start()
        

def getAdministrator():
    url = 'https://www.fakenamegenerator.com/'
    response = requests.get(url, headers=header)
    soup = BeautifulSoup(response.text, "html.parser")
    fullname = soup.find_all('div', 'address')
    fullname = soup.find('h3').text
    age = soup.find_all('dl')
    age = age[6].find('dd').text[:2]
    return fullname, age
    

def writeAdministrators(mul, number):
    global data
    t = time()
    for i in range(number):
        fullname, age = getAdministrator()
        it = number*mul + i
        data[it] = f'{it+1},"{fullname}",{int(age)},{randint(1,10)}\n'
    print((time()-t)/number)

    
def main():
    create_threads()
    while(len(enumerate()) > 1):
        sleep(1)
    print('ending')
    with open(base_path + 'administrators.csv', 'a+') as file:
        for string in data:
            file.write(string)
    file.close()
    

if __name__ == '__main__':
    main()
