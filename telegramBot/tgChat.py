import telebot
import pymysql
from datetime import date
import sqlUserClasses as sql
from app_logger import get_logger

# Подключение к БД
connection = pymysql.connect(host='localhost',
                             port=3306,
                             user='root',
                             password='uztHz6iy56O!GNCVD8Cl',
                             db='keyshopdatabase')

# Логирование
message_logger = get_logger(__name__, 'chatbot.log')
bot = telebot.TeleBot('1590052173:AAH7zAZzaEdmZpGOk_-veYGWrjj-7AYqE7k')

# Клавиатура
keyboard = telebot.types.ReplyKeyboardMarkup(True)
keyboard.row('/start')

# Пользователь
user = sql.User(-1, -1, connection)
login, password = '', ''


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    global user, connection
    if user.status == 'User' or message.text == '/start':
        user = sql.Stranger(message.from_user.id, message.from_user.username, connection)
        bot.send_message(message.from_user.id,
                         f'Приветствую, {message.from_user.first_name}. Войдите в личный кабинет или зарегистрируйтесь',
                         reply_markup=user.keyboard)
        message_logger.info(
            f'Действие: Новый пользователь @{message.from_user.username}, id: {str(message.from_user.id)}, {message.from_user.first_name} {message.from_user.last_name}')
    elif message.text == 'Выход' and (user.status in ['Client', 'Admin']):
        user = sql.Stranger(message.from_user.id, message.from_user.username, connection)
        bot.send_message(message.from_user.id, 'Вы вышли из личного кабинета', reply_markup=user.keyboard)
    elif user.status == 'Stranger':
        if message.text == 'Регистрация':
            bot.send_message(message.from_user.id, 'Введите новый логин и пароль через пробел')
            bot.register_next_step_handler(message, registration)
        elif message.text == 'Вход':
            bot.send_message(message.from_user.id, 'Введите логин и пароль через пробел')
            bot.register_next_step_handler(message, login)
    elif user.status == 'Client':
        if message.text == 'Изменить пароль':
            bot.send_message(message.from_user.id, 'Введите новый пароль')
            bot.register_next_step_handler(message, change_password)
        elif message.text == 'Мой баланс':
            balance = user.get_balance()
            bot.send_message(message.from_user.id, f'Ваш баланс: {balance}')
        elif message.text == 'Пополнить баланс':
            bot.send_message(message.from_user.id, 'Введите сумму пополнения')
            bot.register_next_step_handler(message, add_balance)
        elif message.text == 'Инфо об игре':
            bot.send_message(message.from_user.id, 'Введите название игры')
            bot.register_next_step_handler(message, get_game_info)
        elif message.text == 'Купить игру':
            bot.send_message(message.from_user.id, 'Раздел в разработке...')
            bot.send_message(message.from_user.id, 'Введите данные игры через запятую: название игры и способ оплаты')
            bot.register_next_step_handler(message, buy_game)
        elif message.text == 'Мои отзывы':
            print_appeals(message)
        elif message.text == 'Удалить отзыв':
            bot.send_message(message.from_user.id, 'Введите номер отзыва')
            bot.register_next_step_handler(message, delete_appeal)
    elif user.status == 'Admin':
        if message.text == 'Добавить игру':
            bot.send_message(message.from_user.id, 'Введите данные игры через запятую:\nназвание, дата релиза, цена, id разработчика')
            bot.register_next_step_handler(message, add_game)
        elif message.text == 'Сводка затрат клиентов':
            bot.send_message(message.from_user.id, 'Введите номер месяца')
            bot.register_next_step_handler(message, print_expensive_list)
    else:
        bot.send_message(message.from_user.id, user.status)
        bot.send_message(message.from_user.id, 'Жаль, не понимаю тебя')
        message_logger.info(f'Действие: Сообщение @{message.from_user.username} "{message.text}"')


def registration(message):
    global user, connection
    if len(message.text.split(' ')) == 2:
        login, password = message.text.split(' ')
        if not user.is_taken_login(login):
            bot.send_message(message.from_user.id, 'Регистрация прошла успешно')
            user.create_client(login, password)
        else:
            bot.send_message(message.from_user.id, 'Логин уже занят')
    else:
        bot.send_message(message.from_user.id, 'Некорректные данные')


def login(message):
    global user, connection
    if len(message.text.split(' ')) == 2:
        login, password = message.text.split(' ')
        if login == 'admin' and password == 'root':
            user = sql.Admin(message.from_user.id, message.from_user.username, connection)
            bot.send_message(message.from_user.id, 'Вы администратор', reply_markup=user.keyboard)
        elif user.is_taken_login(login):
            if user.is_valid_password(login, password):
                user = sql.Client(message.from_user.id, message.from_user.username, login, connection)
                bot.send_message(message.from_user.id, f'Привет {user.login}', reply_markup=user.keyboard)
            else:
                bot.send_message(message.from_user.id, 'Неверный пароль')
        else:
            bot.send_message(message.from_user.id, 'Логин не найден')
    else:
        bot.send_message(message.from_user.id, 'Некорректные данные')


def change_password(message):
    global user, connection
    user.change_password(message.text)
    bot.send_message(message.from_user.id, 'Пароль успешно изменен')


def add_balance(message):
    global user, connection
    if message.text.isnumeric():
        user.add_balance(int(message.text))
        bot.send_message(message.from_user.id, f'Зачислено на ваш счет: {message.text}')
    else:
        bot.send_message(message.from_user.id, 'Некорректные данные')


def get_game_info(message):
    global user, connection
    if user.is_game_exists(message.text):
        game = user.get_game(message.text)[0]
        info = f'ID: {game[0]}\n' \
               f'Название: {game[1]}\n' \
               f'Дата релиза: {game[2]}\n' \
               f'Цена: {game[3]}\n' \
               f'Разработчик: {game[4]}'
        bot.send_message(message.from_user.id, info)
    else:
        bot.send_message(message.from_user.id, 'Игра не найдена')


def print_appeals(message):
    global user, connection
    appeals = user.get_appeals()
    if len(appeals) > 0:
        for appeal in appeals:
            text = f"Номер: {appeals.index(appeal) + 1}\n" \
                   f"ID: {appeal[0]}\n" \
                   f"Дата: {appeal[1].strftime('%d %B %Y')}\n" \
                   f"Текст: {appeal[2]}\n" \
                   f"Статус: {appeal[3]}"
            bot.send_message(message.from_user.id, text)
    else:
        bot.send_message(message.from_user.id, f'Вы не оставляли отзывов')


def delete_appeal(message):
    global user, connection
    appeals = user.get_appeals()
    if len(appeals) > 0:
        if message.text.isnumeric():
            if int(message.text) in range(1, len(appeals) + 1):
                user.delete_appeal(appeals[int(message.text) - 1][0])
                bot.send_message(message.from_user.id, 'Отзыв удален')
            else:
                bot.send_message(message.from_user.id, 'Неверный номер отзыва')
        else:
            bot.send_message(message.from_user.id, 'Некорректные данные')
    else:
        bot.send_message(message.from_user.id, f'Вы не оставляли отзывов')


def print_expensive_list(message):
    global user, connection
    if message.text.isnumeric():
        if int(message.text) in range(1, 13):
            ex_list = user.get_expensive_list(int(message.text))
            text = 'Список затрат клиентов:\n'
            for ex in ex_list:
                text = f"ID клиента: {ex[0]}\n" \
                        f"Логин: {ex[1]}\n" \
                        f"Затраты за месяц: {ex[2]}\n" \
                        f"Даты покупок: {ex[3]}"
                bot.send_message(message.from_user.id, text)
        else:
            bot.send_message(message.from_user.id, 'Неверный номер месяца')
    else:
        bot.send_message(message.from_user.id, 'Некорректные данные')


def add_game(message):
    data = message.text.split(',')
    user.add_game(data)
    bot.send_message(message.from_user.id, 'Игра добавлена')


def buy_game(message):
    global user, connection
    game_title, payment = message.text.split(',')
    if user.is_game_exists(game_title):
        game = user.get_game(game_title)[0]
        if user.get_balance() > game[3]:
            keys = user.find_free_keys(game[0])
            if len(keys) > 0:
                key_id = keys[0][0]
                data = [date.today(), payment]
                order_id = user.create_order(data)
                bot.send_message(message.from_user.id, 'Заказ успешно создан')
                order_id = user.get_last_order()
                print(order_id, key_id)
                user.buy_key(key_id, order_id)
                bot.send_message(message.from_user.id, 'Ключ приобретен')
                user.reduce_balance(game[3])
                bot.send_message(message.from_user.id, 'Деньги списаны')
            else:
                bot.send_message(message.from_user.id, 'Нет ключей для данной игры\nОбратитесь в ТП')
        else:
            bot.send_message(message.from_user.id, 'Недостаточно средств для совершения покупки')
    else:
        bot.send_message(message.from_user.id, 'Игра не найдена')


bot.polling(none_stop=True, interval=0)
