import telebot


class User:
    def __init__(self, id, name, connection):
        self.status = 'User'
        self.id = id
        self.name = name
        self.connection = connection
        self.cursor = self.connection.cursor()
        self.keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self.keyboard.row('/start')


class Stranger(User):
    def __init__(self, id, name, connection):
        super().__init__(id, name, connection)
        self.status = 'Stranger'
        self.keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self.keyboard.row('Регистрация', 'Вход')

    def get_login(self, login):
        self.cursor.execute("SELECT login FROM clients WHERE login = %s;", (login,))
        return self.cursor.fetchall()

    def is_taken_login(self, login):
        rows = self.get_login(login)
        return len(rows) > 0

    def get_password(self, login):
        self.cursor.execute("SELECT password FROM clients WHERE login = %s;", (login,))
        return self.cursor.fetchall()

    def is_valid_password(self, login, password):
        rows = self.get_password(login)
        if len(rows) > 0:
            real_password = rows[0][0]
        return password == real_password

    def create_client(self, login, password):
        self.cursor.execute("INSERT INTO clients (login, password, balance) VALUES (%s, %s, 0);", (login, password,))
        self.connection.commit()


class Client(User):
    def __init__(self, id, name, login, connection):
        super().__init__(id, name, connection)
        self.status = 'Client'
        self.keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self.keyboard.row('Изменить пароль', 'Мой баланс', 'Пополнить баланс', 'Инфо об игре')
        self.keyboard.row('Купить игру', 'Мои отзывы', 'Удалить отзыв', 'Выход')
        self.login = login

    def get_client_id(self):
        self.cursor.execute("select client_id from clients "
                            "where login = %s;", (self.login,))
        return self.cursor.fetchall()[0][0]

    def change_password(self, password):
        self.cursor.execute("UPDATE clients SET password = %s WHERE (login = %s);", (password, self.login,))
        self.connection.commit()

    def get_balance(self):
        self.cursor.execute("SELECT balance FROM clients WHERE login = %s;", (self.login,))
        return self.cursor.fetchall()[0][0]

    def add_balance(self, money):
        balance = self.get_balance()
        self.cursor.execute("UPDATE clients SET balance = %s WHERE (login = %s);", (balance+money, self.login,))
        self.connection.commit()

    def reduce_balance(self, money):
        balance = self.get_balance()
        self.cursor.execute("UPDATE clients SET balance = %s WHERE (login = %s);", (balance-money, self.login,))
        self.connection.commit()

    def get_game(self, title):
        self.cursor.execute("SELECT game_id, title, release_date, price, name FROM games "
                            "JOIN developers USING(developer_id) "
                            "WHERE title = %s;", (title,))
        return self.cursor.fetchall()

    def is_game_exists(self, title):
        rows = self.get_game(title)
        return len(rows) > 0

    def get_appeals(self):
        self.cursor.execute("SELECT appeal_id, appeal_date, text, status FROM clients "
                            "JOIN appeals USING(client_id) "
                            "WHERE login = %s;", (self.login,))
        return self.cursor.fetchall()

    def delete_appeal(self, number):
        self.cursor.execute("DELETE FROM appeals WHERE appeal_id = %s;", (number,))
        self.connection.commit()

    def find_free_keys(self, id):
        self.cursor.execute("select key_id, title from keyz "
                            "join keys_packs using(pack_id) "
                            "join games using(game_id) "
                            "where order_id is null and game_id=%s;", (id,))
        return self.cursor.fetchall()

    def create_order(self, data):
        self.cursor.execute("INSERT INTO orders (date, payment_method, status, client_id, discount_id) "
                            "VALUES (%s, %s, '1', %s, '1');", (data[0], data[1], self.get_client_id()))
        self.connection.commit()

    def buy_key(self, key_id, order_id):
        self.cursor.execute("UPDATE keyz SET order_id = %s WHERE (key_id = %s);", (order_id, key_id,))
        self.connection.commit()

    def get_last_order(self):
        self.cursor.execute("select order_id from orders "
                            "order by order_id desc "
                            "limit 1;")
        return self.cursor.fetchall()[0][0]


class Admin(User):
    def __init__(self, id, name, connection):
        super().__init__(id, name, connection)
        self.status = 'Admin'
        self.keyboard = telebot.types.ReplyKeyboardMarkup(True)
        self.keyboard.row('Добавить игру', 'Сводка затрат клиентов', 'Выход')

    def get_expensive_list(self, month):
        self.cursor.execute("SELECT client_id, login, ROUND(SUM(games.price*(1-amount/100))), "
                            "GROUP_CONCAT(distinct orders.date) FROM clients "
                            "JOIN orders USING(client_id) "
                            "JOIN discounts USING(discount_id) "
                            "JOIN keyz USING(order_id) "
                            "JOIN keys_packs USING(pack_id) "
                            "JOIN games USING(game_id) "
                            "WHERE orders.date BETWEEN '2021-%s-01' AND '2021-%s-31' "
                            "GROUP BY(client_id) LIMIT 5;", (month, month,))
        return self.cursor.fetchall()

    def add_game(self, data):
        self.cursor.execute("INSERT INTO games(title, release_date, price, developer_id) "
                            "VALUES(%s, %s, %s, %s);", (data[0], data[1], data[2], data[3],))
        self.connection.commit()
